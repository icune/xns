<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Transaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->id();
            $table->string('currency_id', 10)->index();
            $table->foreign('currency_id')->references('id')->on('currency');
            $table->bigInteger('wallet_from_id')->nullable()->index();
            $table->foreign('wallet_from_id')->references('id')->on('wallet');
            $table->bigInteger('wallet_to_id')->nullable()->index();
            $table->foreign('wallet_to_id')->references('id')->on('wallet');
            $table->double('amount', 30, 18);
            $table->date('ds')->index()->default(DB::raw('CURRENT_DATE'));
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transaction');
    }
}
