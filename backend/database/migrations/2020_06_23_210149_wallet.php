<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Wallet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wallet', function (Blueprint $table) {
            $table->id();
            $table->string('currency_id', 10)->index();
            $table->foreign('currency_id')->references('id')->on('currency');
            $table->bigInteger('user_id')->index();
            $table->foreign('user_id')->references('id')->on('users');
            $table->double('balance', 30, 18);
            $table->timestamps();
            $table->unique(['user_id', 'currency_id']);
        });

        DB::statement('
            ALTER TABLE wallet ADD CONSTRAINT balance_positive CHECK (balance >= 0)
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('wallet');
    }
}
