<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CurrencyRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('currency_rate', function (Blueprint $table) {
            $table->string('currency_id', 10)->index();
            $table->foreign('currency_id')->references('id')->on('currency');
            $table->double('rate_usd', 30, 18);
            $table->date('ds')->index()->default(DB::raw('CURRENT_DATE'));
            $table->unique(['user_id', 'ds']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('currency_rate');
    }
}
