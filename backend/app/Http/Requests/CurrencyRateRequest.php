<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CurrencyRateRequest extends FormRequest {
    public function rules()
    {
        return [
            'currency' => 'required|string',
            'rate_usd' => 'required|string',
            'date' => 'required|date',
        ];
    }
}
