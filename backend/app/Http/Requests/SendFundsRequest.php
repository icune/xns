<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendFundsRequest extends FormRequest {
    public function rules()
    {
        return [
            'from_name' => 'required|string',
            'to_name' => 'required|string',
            'currency' => 'required|string',
            'amount' => 'required|numeric'
        ];
    }
}
