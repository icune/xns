<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DepositUserRequest extends FormRequest {
    public function rules()
    {
        return [
            'name' => 'required|string',
            'currency' => 'required|string',
            'amount' => 'required|numeric'
        ];
    }
}
