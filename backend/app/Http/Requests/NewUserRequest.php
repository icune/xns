<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewUserRequest extends FormRequest {
    public function rules()
    {
        return [
            'name' => 'required|string',
            'country' => 'string',
            'city' => 'string',
            'currency' => 'required|string'
        ];
    }
}
