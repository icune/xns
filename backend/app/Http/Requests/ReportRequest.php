<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReportRequest extends FormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|string',
            'currency' => 'required|string',
            'from' => 'date',
            'to' => 'date',
            'format' => 'string'
        ];
    }
}
