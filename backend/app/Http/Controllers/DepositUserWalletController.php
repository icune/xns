<?php

namespace App\Http\Controllers;

use App\Database\Repository\TransactionRepositoryContract;
use App\Http\Requests\DepositUserRequest;
use App\Http\Requests\SendFundsRequest;

class DepositUserWalletController extends Controller
{
    public function __invoke(
        DepositUserRequest $request,
        TransactionRepositoryContract $transactionRepository)
    {
        try {
            $sendRequest = new SendFundsRequest();
            $sendRequest->replace([
                'from_name' => 'su@root.com',
                'to_name' => $request->get('name'),
                'currency' => $request->get('currency'),
                'amount' => $request->get('amount')

            ]);
            $transactionRepository->transfer($sendRequest);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
        return response()->json([]);
    }
}
