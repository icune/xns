<?php

namespace App\Http\Controllers;

use App\Database\Repository\CurrencyRepositoryContract;
use App\Http\Requests\CurrencyRateRequest;

class SetCurrencyRateController extends Controller
{
    public function __invoke(
        CurrencyRateRequest $request,
        CurrencyRepositoryContract $currencyRepository)
    {
        try {
            $currencyRepository->createNew($request);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
        return response()->json([]);
    }
}
