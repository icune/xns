<?php

namespace App\Http\Controllers;

use App\Database\Repository\TransactionRepositoryContract;
use App\Http\Requests\ReportRequest;

class ReportController extends Controller
{
    public function __invoke(
        ReportRequest $request,
        TransactionRepositoryContract $transactionRepository)
    {
        try {
            $report = $transactionRepository->report($request);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }

        if ($request->get('format') == 'csv') {
            return responnse()->csv(array_merge($report['rows'], $report['sum']));
        } else {
            return response()->json($report);
        }

    }
}
