<?php

namespace App\Http\Controllers;

use App\Database\Repository\TransactionRepositoryContract;
use App\Http\Requests\SendFundsRequest;

class SendFundsController extends Controller
{
    public function __invoke(
        SendFundsRequest $request,
        TransactionRepositoryContract $transactionRepository)
    {
        try {
            $transactionRepository->transfer($request);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
        return response()->json([]);
    }
}
