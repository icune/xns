<?php

namespace App\Http\Controllers;

use App\Database\Repository\UserRepositoryContract;
use App\Http\Requests\NewUserRequest;

class UserRegisterController extends Controller
{
    public function __invoke(
        NewUserRequest $request,
        UserRepositoryContract $userRepository)
    {
        try {
            $userRepository->createNew($request);
        } catch (\Exception $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        }
        return response()->json([]);
    }
}
