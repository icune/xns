<?php

namespace App\Console\Commands;

use App\Database\Model\User;
use Illuminate\Foundation\Console\ModelMakeCommand;

class CustomMakeModelCommand extends ModelMakeCommand
{
    protected function getDefaultNamespace($rootNamespace)
    {
        return implode('\\', array_slice(explode('\\', User::class), 0, -1));
    }
}
