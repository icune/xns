<?php

namespace App\Providers;

use App\Database\Repository\CurrencyRepository;
use App\Database\Repository\CurrencyRepositoryContract;
use App\Database\Repository\TransactionRepository;
use App\Database\Repository\TransactionRepositoryContract;
use App\Database\Repository\UserRepository;
use App\Database\Repository\UserRepositoryContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(UserRepositoryContract::class, UserRepository::class);
        $this->app->bind(TransactionRepositoryContract::class, TransactionRepository::class);
        $this->app->bind(CurrencyRepositoryContract::class, CurrencyRepository::class);
    }
}
