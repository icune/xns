<?php

namespace App\Database\Repository;

use App\Database\Model\Currency;
use App\Database\Model\CurrencyRate;
use App\Database\Model\User;
use App\Database\Model\Wallet;
use App\Http\Requests\CurrencyRateRequest;
use App\Http\Requests\NewUserRequest;

class CurrencyRepository implements CurrencyRepositoryContract {
    public function createNew(CurrencyRateRequest $request)
    {
        CurrencyRate::create([
            'currency_id' => $request->get('currency'),
            'rate_usd' => $request->get('rate_usd'),
            'date' => $request->get('date'),
        ]);
    }
}
