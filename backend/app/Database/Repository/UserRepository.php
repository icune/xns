<?php

namespace App\Database\Repository;

use App\Database\Model\Currency;
use App\Database\Model\User;
use App\Database\Model\Wallet;
use App\Http\Requests\NewUserRequest;

class UserRepository implements UserRepositoryContract {
    public function createNew(NewUserRequest $request)
    {
        $currency = Currency::find($request->get('currency'));

        if (!$currency) {
            throw new \Exception('No such currency');
        }
        try {
            $user = User::create([
                'password' => 'mock',
                'email' => $request->get('name'),
                'country' => $request->get('country'),
                'city' => $request->get('city'),
            ]);
        } catch (\Exception $e) {
            throw new \Exception('User already exists');
        }

        try {
            Wallet::create([
                'user_id' => $user->id,
                'currency_id' => $request->get('currency'),
                'balance' => 0
            ]);
        } catch (\Exception $e) {
            throw new \Exception('Wallet already exists');
        }
    }
}
