<?php

namespace App\Database\Repository;

use App\Http\Requests\ReportRequest;
use App\Http\Requests\SendFundsRequest;

interface TransactionRepositoryContract {
    public function transfer(SendFundsRequest $request);
    public function report(ReportRequest $request);
}
