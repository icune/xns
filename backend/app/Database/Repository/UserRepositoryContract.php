<?php

namespace App\Database\Repository;

use App\Http\Requests\NewUserRequest;

interface UserRepositoryContract {
    public function createNew(NewUserRequest $request);
}
