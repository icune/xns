<?php

namespace App\Database\Repository;

use App\Database\Model\Currency;
use App\Database\Model\Transaction;
use App\Database\Model\User;
use App\Database\Model\Wallet;
use App\Http\Requests\ReportRequest;
use App\Http\Requests\SendFundsRequest;
use Illuminate\Support\Facades\DB;

class TransactionRepository implements TransactionRepositoryContract {
    public function transfer(SendFundsRequest $request)
    {
        $amount = $request->get('amount');

        $currency = Currency::find($request->get('currency'));


        if (!$currency) {
            throw new \Exception('No such currency');
        }

        $fromUser = User::where(['email' => $request->get('from_name')])->first();

        if (!$fromUser) {
            throw new \Exception('From user not found');
        }

        $toUser = User::where(['email' => $request->get('to_name')])->first();

        if (!$toUser) {
            throw new \Exception('From user not found');
        }

        $fromWallet = Wallet::where([
            'user_id' => $fromUser->id,
            'currency_id' => $request->get('currency')
        ])->first();



        if (!$fromWallet) {
            throw new \Exception('From wallet not found');
        }


        if (floatval($fromWallet->balance) < floatval($amount)) {
            throw new \Exception('Not enough balance');
        }

        $toWallet = Wallet::where([
            'user_id' => $toUser->id,
            'currency_id' => $request->get('currency')
        ])->first();

        if (!$toWallet) {
            $toWallet = Wallet::create([
                'user_id' => $toUser->id,
                'currency_id' => $request->get('currency'),
                'balance' => 0
            ]);
        }


        try {
            DB::beginTransaction();
        } catch (\Exception $e) {
            throw new \Exception('Unknown error');
        }

        try {
            DB::statement('LOCK TABLE wallet IN ROW EXCLUSIVE MODE;');

            Transaction::create([
                'wallet_from_id' => $fromWallet->id,
                'wallet_to_id' => $toWallet->id,
                'amount' => $amount,
                'currency_id' => $currency->id
            ]);

            DB::statement('
                UPDATE wallet SET balance = balance - :amount WHERE id = :from_id
            ', ['from_id' => $fromWallet->id, 'amount' => $amount]);

            DB::statement('
                UPDATE wallet SET balance = balance + :amount WHERE id = :to_id
            ', ['to_id' => $toWallet->id, 'amount' => $amount]);

            DB::commit();
        } catch (\Exception $e) {
            try {
                DB::rollBack();
            } catch (\Exception $e) {
                throw new \Exception('Fatal error - call the police');
            }

            throw $e;
        };
    }


    public function report(ReportRequest $request)
    {
        $attrs = [
            'name' => $request->get('name'),
            'currency' => $request->get('currency')
        ];

        $sql = '
            SELECT * FROM
            transaction tr
            LEFT JOIN wallet wl_from ON wl_from.id = tr.wallet_from_id
            LEFT JOIN users u_from ON u_from.id = wl_from.user_id
            LEFT JOIN wallet wl_to ON wl_to.id = tr.wallet_to_id
            LEFT JOIN users u_to ON u_to.id = wl_to.user_id
            WHERE
        ';

        $commonWhere = '(u_to.email = :name OR u_from.email = :name) AND tr.currency_id=:currency';

        $sqlAdditions = [];


        if ($request->get('from')) {
            $sqlAdditions []= ' AND tr.ds >= :from';
            $attrs['from'] = $request->get('from');
        }

        if ($request->get('to')) {
            $sqlAdditions []= ' AND tr.ds <= :to';
            $attrs['to'] = $request->get('to');
        }

        $additions = $commonWhere . (count($sqlAdditions) ? ' AND ' : '') . implode(' AND ', $sqlAdditions);

        $ret = [];
        $ret['rows'] =  DB::select($sql . $additions, $attrs);



        $sqlSum = '
            SELECT SUM(tr.amount * cr.rate_usd) sum FROM
            transaction tr
            LEFT JOIN wallet wl_from ON wl_from.id = tr.wallet_from_id
            LEFT JOIN users u_from ON u_from.id = wl_from.user_id
            LEFT JOIN wallet wl_to ON wl_to.id = tr.wallet_to_id
            LEFT JOIN users u_to ON u_to.id = wl_to.user_id
            LEFT JOIN currency_rate cr ON (cr.currency_id=:currency AND cr.ds = tr.ds)
            WHERE
        ';

        $ret['sum'] =  DB::select($sqlSum . $additions, $attrs);

        return $ret;
    }
}
