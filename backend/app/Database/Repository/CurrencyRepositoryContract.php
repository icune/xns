<?php

namespace App\Database\Repository;

use App\Http\Requests\CurrencyRateRequest;

interface CurrencyRepositoryContract {
    public function createNew(CurrencyRateRequest $request);
}
