<?php

namespace App\Database\Seed;

use Illuminate\Database\Seeder;
use \App\Database\Model\Currency;

class CurrencySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Currency::insert([
            [
                'id' => 'rub',
                'name' => 'Ruble'
            ],
            [
                'id' => 'eur',
                'name' => 'Euro'
            ],
        ]);
    }
}
