<?php

namespace App\Database\Seed;

use App\Database\Model\User;
use App\Database\Model\Wallet;
use Illuminate\Database\Seeder;
use \App\Database\Model\Currency;

class InitSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [
            [
                'id' => 'rub',
                'name' => 'Ruble'
            ],
            [
                'id' => 'eur',
                'name' => 'Euro'
            ]
        ];
        try {
            Currency::insert(
                $currencies
            );
        } catch (\Exception $e) {

        };

        $su = User::create([
            'email' => 'su@root.com',
            'password' => 'mock'
        ]);

        foreach ($currencies as $currency) {
            Wallet::create([
                'user_id' => $su->id,
                'currency_id' => $currency['id'],
                'balance' => 100000000000000,
            ]);
        }
    }
}
