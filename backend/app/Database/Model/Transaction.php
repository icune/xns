<?php

namespace App\Database\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Database\Model\Transaction
 *
 * @property int $id
 * @property string $currency_id
 * @property int|null $wallet_from_id
 * @property int|null $wallet_to_id
 * @property float $amount
 * @property string $ds
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction whereDs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction whereWalletFromId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Transaction whereWalletToId($value)
 * @mixin \Eloquent
 */
class Transaction extends Model
{
    const TABLE_NAME = 'transaction';
    protected $table = self::TABLE_NAME;

    protected $fillable = ['currency_id', 'wallet_from_id', 'wallet_to_id', 'amount'];
}
