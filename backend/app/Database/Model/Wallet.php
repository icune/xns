<?php

namespace App\Database\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Database\Model\Wallet
 *
 * @property int $id
 * @property string $currency_id
 * @property int $user_id
 * @property float $balance
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Wallet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Wallet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Wallet query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Wallet whereBalance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Wallet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Wallet whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Wallet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Wallet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Wallet whereUserId($value)
 * @mixin \Eloquent
 */
class Wallet extends Model
{
    const TABLE_NAME = 'wallet';
    protected $table = self::TABLE_NAME;

    protected $fillable = ['user_id', 'currency_id', 'balance'];
}
