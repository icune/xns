<?php

namespace App\Database\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Database\Model\Currency
 *
 * @property int $id
 * @property string|null $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Currency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Currency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Currency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Currency whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Currency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Currency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\Currency whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Currency extends Model
{
    const TABLE_NAME = 'currency';
    protected $table = self::TABLE_NAME;

    protected $primaryKey = 'id';
    public $incrementing = false;
    protected $keyType = 'string';
}
