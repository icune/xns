<?php

namespace App\Database\Model;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Database\Model\CurrencyRate
 *
 * @property string $currency_id
 * @property float $rate_usd
 * @property string $ds
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\CurrencyRate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\CurrencyRate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\CurrencyRate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\CurrencyRate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\CurrencyRate whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\CurrencyRate whereDs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\CurrencyRate whereRateUsd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Database\Model\CurrencyRate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CurrencyRate extends Model
{
    const TABLE_NAME = 'currency_rate';
    protected $table = self::TABLE_NAME;

    protected $primaryKey = null;
    public $incrementing = false;
//    protected $keyType = 'string';

    protected $fillable = ['currency_id', 'rate_usd', 'ds'];
}
