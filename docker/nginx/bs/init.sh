#!/bin/bash
ENVS='${XW_HOST}'
envsubst "$ENVS" < /tmp/nginx/template.conf > /etc/nginx/conf.d/default.conf
exec nginx -g 'daemon off;'
