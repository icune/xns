#!/bin/bash

if [[ $WITH_INSTALL == "true" ]]
then
  cd /var/www/backend
  composer install
  php artisan key:generate
  php artisan migrate
  php artisan db:seed
fi

mkdir /run/php
php-fpm7.4 -F